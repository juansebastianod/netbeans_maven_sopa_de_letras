package Negocio;

import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * Write a description of class SopaDeLetras here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class SopaDeLetras {

    // instance variables - replace the example below with your own
    private char sopas[][];

    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras() {

    }

    public SopaDeLetras(String palabras) throws Exception {
        if (palabras == null || palabras.isEmpty()) {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }

        //Crear la matriz con las correspondientes filas:
        String palabras2[] = palabras.split(",");
        this.sopas = new char[palabras2.length][];
        int i = 0;
        for (String palabraX : palabras2) {
            //Creando las columnas de la fila i
            this.sopas[i] = new char[palabraX.length()];
            pasar(palabraX, this.sopas[i]);
            i++;

        }

    }

    private void pasar(String palabra, char fila[]) {

        for (int j = 0; j < palabra.length(); j++) {
            fila[j] = palabra.charAt(j);
        }
    }

    public String toString() {
        String msg = "";
        for (int i = 0; i < this.sopas.length; i++) {
            for (int j = 0; j < this.sopas[i].length; j++) {
                msg += this.sopas[i][j] + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    public String toString2() {
        String msg = "";
        for (char filas[] : this.sopas) {
            for (char dato : filas) {
                msg += dato + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    public boolean esCuadrada()throws Exception {
       if(sopas==null)
        {
            throw new Exception("la matriz esta vacia");
        }

        int a=0;
        boolean  comparar;
        for(int i=0;i<sopas.length;i++){
            if(sopas[i].length==sopas.length){a++;}

        }
        comparar=a==sopas.length;
        return comparar;
    }

    public boolean esDispersa()throws Exception {
       if(sopas==null)
        {
            throw new Exception("la matriz esta vacia");
        }

        int j=1;
        boolean dispersa=true;
        int contador=0;
        for(int i=0;i+1<sopas.length;i++){
            if(j!=sopas.length){
                if(sopas[i].length!=sopas[j].length){
                    contador++;
                }
            }j++;
        }
        dispersa=contador!=0;

        return dispersa;
    }

    public boolean esRectangular()throws Exception {
        if(sopas==null)
        {
            throw new Exception("la matriz esta vacia");
        }
        if(!esDispersa()&&!esCuadrada()){return true;}else{
            return false;}
    }
    /*
        retorna cuantas veces esta la palabra en la matriz
     */
    public int getContar(String palabra)throws Exception {
        if(sopas==null)
        {
            throw new Exception("la matriz esta vacia");
        }
        

        int cantidad = 0;
        String palabraM;
        int contar = 0;
        int maximo = palabra.length();
        
        for (char vector[] : this.sopas) {
            String juntar = "";
            for (char vector2 : vector) {
                juntar += vector2;
            }
            
            
            while(maximo+contar <= juntar.length()){

                palabraM = juntar.substring(contar, contar + maximo);
                if (palabraM.equals(palabra)) {
                    cantidad++;
                }
                contar++;
            }
            contar = 0;
        }
        return cantidad;
    }

    /*
        debe ser cuadrada sopas
     */
    public char[] getDiagonalPrincipal() throws Exception {
        if(esDispersa()|| esRectangular()){throw new Exception("la matriz no es  cuadrada");}

        char diagonalPrincipal []=new char[sopas.length];

        for(int i=0;i<sopas.length;i++){
            diagonalPrincipal[i]= sopas[i][i];

        }

        return diagonalPrincipal;
    }

    //Start GetterSetterExtension Source Code
    /**
     * GET Method Propertie sopas
     */
    public char[][] getSopas() {
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
//!
    private static void leerExcel(String file) throws Exception {
        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream("file"));
        //Obtiene la hoja 1
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum() + 1;
        System.out.println("Filas:" + canFilas);
        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila
            int cantCol = filas.getLastCellNum();

            for (int j = 0; j < cantCol; j++) {
                //Obtiene la celda y su valor respectivo
                //double r=filas.getCell(i).getNumericCellValue();
                String valor = filas.getCell(j).getStringCellValue();
                System.out.print(valor + "\t");
            }
            System.out.println();
        }
    }
    
    public static void main(String[] args) throws Exception {
        leerExcel("data.xls");
    }
}
